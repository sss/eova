/**
 * Copyright (c) 2013-2015, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.engine;

import java.util.ArrayList;
import java.util.List;

import com.eova.common.utils.xx;
import com.eova.model.MetaField;
import com.eova.model.MetaObject;

public class EovaExp {
	public static void main(String[] args) {
		String exp = "select id ID,name 昵称 from game where 1=1;ds=main";

		System.out.println("select="+getSelectNoAlias(exp));
		System.out.println("from="+getFrom(exp));
		System.out.println("where="+getWhere(exp));
		System.out.println("ds="+getDs(exp));
		System.out.println("pk="+getPk(exp));
		System.out.println("cn="+getCn(exp));
		
		System.out.println("sa="+getSelectItem(exp));
	}

	public static String getWhere(String exp){
		int where = exp.indexOf("where");
		if (where == -1) {
			return "";
		}
		int end = exp.indexOf(";");
		return exp.substring(where, end);
	}
	
	/**
	 * 获取数据源
	 * @return
	 */
	public static String getDs(String exp){
		int a = exp.indexOf(";ds=");
		if (a == -1) {
			return xx.DS_MAIN;
		}
		a += 4;
		return exp.substring(a, exp.length());
	}
	
	/**
	 * 获取主键
	 * @return
	 */
	public static String getPk(String exp){
		String[] items = getItemNoAlias(exp).split(",");
		return items[0];
	}

	/**
	 * 获取中文名, 第二列为CN
	 * @return
	 */
	public static String getCn(String exp){
		String[] items = getItemNoAlias(exp).split(",");
		return items[1];
	}
	
	/**
	 * 获取完整的select 部分
	 * eg:select id,name,value
	 * @param exp
	 * @return
	 */
	public static String getSelect(String exp){
		return "select " + getSelectItem(exp) +" ";
	}
	
	/**
	 * 获取没有别名的select 部分
	 * eg:select id,name,value
	 * @param exp
	 * @return
	 */
	public static String getSelectNoAlias(String exp){
		return "select " + getItemNoAlias(exp) +" ";
	}
	
	/**
	 * 获取过滤别名后的select项
	 * eg: id,name
	 * @param exp
	 * @return
	 */
	private static String getItemNoAlias(String exp){
		StringBuilder sb = new StringBuilder("");
		// 截取select的Item
		exp = getSelectItem(exp);
		// 过滤掉别名
		for(String str : exp.split(",")){
//			System.out.println(str.trim());
			String[] strs = str.trim().split(" ");
			String en = strs[0];
			//System.out.println(en);
			sb.append(en).append(",");
		}
		sb.delete(sb.length() - 1, sb.length());
		return sb.toString();
	}
	
	/**
	 * 截取select的Item
	 * eg: id xx,name xx
	 * @param exp
	 * @return
	 */
	private static String getSelectItem(String exp){
		int select = exp.indexOf("select") + 7;
		int from = exp.indexOf("from");
		exp = exp.substring(select, from);
		return exp;
	}
	
	public static String getFrom(String exp){
		int from = exp.indexOf("from");
		int where = exp.indexOf("where");
		return exp.substring(from, where); 
	}
	
	/**
	 * 构建元对象
	 * @param exp 表达式
	 * @return
	 */
	public static MetaObject getEo(String exp) {
		// 获取元对象模版
		MetaObject eo = MetaObject.dao.getTemplate();
		eo.put("data_source", getDs(exp));
		eo.put("name", "");
		// 获取第一的值作为主键
		eo.put("pk_name", getPk(exp));
		// 获取第二列的值作为CN
		eo.put("cn", getCn(exp));
		
		return eo;
	}
	
	/**
	 * 构建元字段属性
	 * @param exp 表达式
	 * @return
	 */
	public static List<MetaField> getEis(String exp) {

		// select id id,nickname 昵称,loginid 帐号 from eova_user where id = ?
		int a = 7;
		int b = exp.indexOf("from");

		// id id,nickname 昵称,loginid 帐号
		String items = exp.substring(a, b);

		List<MetaField> fields = new ArrayList<MetaField>();
		int index = 0;
		for (String item : items.split(",")) {
			index++;
			String[] strs = item.split(" ");

			String en = strs[0];
			String cn = strs[1];

			// ei.put(key, value);
			boolean isQuery = true;
			if (index == 1) {
				isQuery = false;
			}
			fields.add(buildItem(index, en, cn, isQuery));
		}
		return fields;
	}

	/**
	 * 手工组装字段元数据
	 * @param index 排序
	 * @param en 英文名
	 * @param cn 中文名
	 * @param isQuery 是否可查询
	 * @return
	 */
	public static MetaField buildItem(int index, String en, String cn, boolean isQuery) {

		en = en.toLowerCase();
		// 获取元模版字段
		MetaField ei = MetaField.dao.getTemplate();
		ei.put("order_num", index);
		ei.put("en", en);
		ei.put("cn", cn);
		ei.put("data_type", "string");
		ei.put("type", "文本框");
		ei.put("is_query", isQuery);
		return ei;
	}
}