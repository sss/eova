/**
 * Copyright (c) 2013-2015, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.template.common.config;

/**
 * 模板相关常量配置
 * 
 * @author Jieven
 * @date 2013-1-3
 */
public class TemplateConfig {

	/** 模版Code-单表 **/
	public static final String SINGLEGRID = "singleGrid";
	/** 模版Code-主子表 **/
	public static final String MASTERSLAVEGRID = "masterSlaveGrid";
	
	/** 字段数据类型-字符 **/
	public static final String DATATYPE_STRING = "string";
	/** 字段数据类型-数值 **/
	public static final String DATATYPE_NUMBER = "number";
	/** 字段数据类型-时间 **/
	public static final String DATATYPE_TIME = "time";

	/** 工具栏按钮类型-dialog **/
	public static final String BUTTON_DIALOG = "dialog";
	/** 工具栏按钮类型-ajaxTodo **/
	public static final String BUTTON_AJAXTODO = "ajaxTodo";
	
	/** 新增处理方法名 **/
	public static final String ADD = "add";
	/** 删除处理方法名 **/
	public static final String DELETE = "delete";
	/** 更新处理方法名 **/
	public static final String UPDATE = "update";
	/** 查询处理方法名 **/
	public static final String LIST = "list";
	/** 导入处理方法名 **/
	public static final String IMPORTXLS = "importXls";
	
}